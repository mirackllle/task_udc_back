package udc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import udc.service.DescriptionService;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

/**
 * Description controller
 * @autor Prorock
 * @version 1.0
 */
@Controller
@CrossOrigin
@RequestMapping("/description")
public class DescriptionController {

    private final transient DescriptionService descriptionSer;

    @Autowired
    public DescriptionController(final DescriptionService descriptionSer) {
        this.descriptionSer = descriptionSer;
    }

    /**
     * Get description method
     * @return ResponseEntity
     */
    @GetMapping()
    public ResponseEntity<?> getDescription() {
       return status(OK).body(descriptionSer.getLastDescription());
    }
}
