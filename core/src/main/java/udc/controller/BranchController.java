package udc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import udc.dto.BranchDto;
import udc.service.BranchService;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

/**
 * Branch controller
 * @autor Prorock
 * @version 1.0
 */
@Controller
@CrossOrigin
@RequestMapping("/branch")
public class BranchController {

    private final transient BranchService branchService;

    @Autowired
    public BranchController(final BranchService branchService) {
        this.branchService = branchService;
    }

    /**
     * Insert new Branch method
     * @param branchDto contains dto object
     * @return ResponseEntity
     */
    @PostMapping("/create")
    public ResponseEntity<?> insertBranch(@RequestBody final @Valid BranchDto branchDto) {
        return status(OK).body(branchService.save(branchDto));
    }

    /**
     * Delete new Branch method
     * @param id - contains id branch object
     * @return ResponseEntity
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBranch(@PathVariable("id") final Long id) {
        branchService.delete(id);
        return new ResponseEntity<>(OK);
    }

    /**
     * Update branch method
     * @param branchDto contains dto object
     * @return ResponseEntity
     */
    @PutMapping("/update")
    public ResponseEntity<?> updateBranch(@RequestBody final @Valid BranchDto branchDto) {
        return status(OK).body(branchService.save(branchDto));
    }
}
