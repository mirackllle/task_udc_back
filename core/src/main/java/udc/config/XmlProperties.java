package udc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * XML property configuration Bean
 * @autor Prorock
 * @version 1.0
 */
@Component
@ConfigurationProperties(prefix = "app.xml")
public class XmlProperties {

    private String fileUrl;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(final String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
