package udc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Log property configuration Bean
 * @autor Prorock
 * @version 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "filter")
public class LogConfigurationProperty {

    public boolean isLogBody() {
        return logBody;
    }

    public void setLogBody(final boolean logBody) {
        this.logBody = logBody;
    }

    private boolean logBody;

}