package udc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;
import udc.filter.CustomRequestResponseLoggingFilter;

/**
 * Log filter configuration Bean
 * @autor Prorock
 * @version 1.0
 */
@Configuration
@SuppressWarnings("PMD")
public class RequestFilterConfig {

    private final LogConfigurationProperty logConfigurationProperty;

    @Autowired
    public RequestFilterConfig(final LogConfigurationProperty property) {
        this.logConfigurationProperty = property;
    }

    @Bean
    public OncePerRequestFilter responseLoggingFilter(){
        return new CustomRequestResponseLoggingFilter(logConfigurationProperty.isLogBody());
    }

}
