package udc.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Log filter
 * @autor Prorock
 * @version 1.0
 */
public class CustomRequestResponseLoggingFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(CustomRequestResponseLoggingFilter.class);

    private transient final boolean logBody;

    public CustomRequestResponseLoggingFilter(final boolean logBody) {
        this.logBody = logBody;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest req,
                                    final HttpServletResponse resp,
                                    final FilterChain filterChain) throws ServletException, IOException {
        final var requestWrapper = new ContentCachingRequestWrapper(req);
        final var responseWrapper = new ContentCachingResponseWrapper(resp);
        filterChain.doFilter(requestWrapper, responseWrapper);

        logRequest(req);
        logResponse(resp);

        if (logBody) {
            logRequestBody(requestWrapper);
            logResponseBody(responseWrapper);
        }

        responseWrapper.copyBodyToResponse();
    }

    /**
     * Method to log http response body.
     *
     * @param responseWrapper contains wrapped HttpServletResponse object
     */
    private void logResponseBody(final ContentCachingResponseWrapper responseWrapper) {
        final var respReader = new BufferedReader(
                new InputStreamReader(
                        new ByteArrayInputStream(responseWrapper.getContentAsByteArray())));
        final var responseBody = respReader.lines().collect(Collectors.joining(System.lineSeparator()));
        LOG.info(String.format("RESPONSE BODY: %s", responseBody));
    }

    /**
     * Method to log http request body.
     *
     * @param requestWrapper contains wrapped HttpServletRequest object
     */
    private void logRequestBody(final ContentCachingRequestWrapper requestWrapper) {
        final var reqReader = new BufferedReader(
                new InputStreamReader(
                        new ByteArrayInputStream(requestWrapper.getContentAsByteArray())));
        final var requestBody = reqReader.lines().collect(Collectors.joining(System.lineSeparator()));
        LOG.info(String.format("REQUEST BODY: %s", requestBody));

    }

    /**
     * Method to log http response.
     *
     * @param response contains HttpServletResponse object
     */
    private void logResponse(final HttpServletResponse response) {
        final var status = HttpStatus.resolve(response.getStatus());
        final var contentType = response.getContentType();
        LOG.info(String.format("SERVER RESPONSE: status: %s, content type: %s",
                status.toString(), contentType));
    }

    /**
     * Method to log http request.
     *
     * @param request contains HttpServletResponse object
     */
    private void logRequest(final HttpServletRequest request) {
        final var requestURI = request.getRequestURI();
        final var method = request.getMethod();
        final var contentType = request.getContentType();
        final var principal = Optional.ofNullable(request.getUserPrincipal()).orElse(() -> "null");
        LOG.info(String.format("CLIENT REQUEST: URI: %s, method: %s, content type: %s, principal: %s",
                requestURI, method, contentType, principal.getName()));
    }


}
