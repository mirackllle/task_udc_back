package udc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import udc.service.XmlService;

@EntityScan("udc")
@SpringBootApplication(scanBasePackages = "udc")
public class ApplicationRunner {

    public static void main(final String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationRunner.class, args);
        context.getBean(XmlService.class).updateDBFromStore();
    }

}