package udc.message;

public enum Message {

    ERROR_XML_TO_DATABASE("could not synchronize data from xml to database"),
    ERROR_DATABASE_TO_XML("could not synchronize data from database to xml");

    private String description;

    Message(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
