package udc.xml;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Date;

@JacksonXmlRootElement( localName = "row")
public class BranchXml {

    @JsonProperty("branch_num")
    private int branchNum;
    @JsonProperty("branch_sum")
    private long branchSum;
    @JsonProperty("branch_lines")
    private int branchLines;
    @JsonProperty("date_pay")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "ddMMyyyy")
    private Date datePay;
    @JsonProperty("num_list")
    private int numList;
    @JsonProperty("file_name")
    private String fileName;
    @JsonProperty("file_data")
    private String fileData;

    public BranchXml() {};

    public BranchXml(final int branchNum, final long branchSum, final int branchLines, final Date datePay,
                     final int numList, final String fileName, final String fileData) {
        this.branchNum = branchNum;
        this.branchSum = branchSum;
        this.branchLines = branchLines;
        this.datePay = datePay;
        this.numList = numList;
        this.fileName = fileName;
        this.fileData = fileData;
    }

    public int getBranchNum() {
        return branchNum;
    }

    public void setBranchNum(final int branchNum) {
        this.branchNum = branchNum;
    }

    public long getBranchSum() {
        return branchSum;
    }

    public void setBranchSum(final long branchSum) {
        this.branchSum = branchSum;
    }

    public int getBranchLines() {
        return branchLines;
    }

    public void setBranchLines(final int branchLines) {
        this.branchLines = branchLines;
    }

    public Date getDatePay() {
        return datePay;
    }

    public void setDatePay(final Date datePay) {
        this.datePay = datePay;
    }

    public int getNumList() {
        return numList;
    }

    public void setNumList(final int numList) {
        this.numList = numList;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(final String fileData) {
        this.fileData = fileData;
    }
}
