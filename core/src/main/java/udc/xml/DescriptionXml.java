package udc.xml;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Date;
import java.util.List;

@JacksonXmlRootElement( localName = "description_files")
public class DescriptionXml {

    @JsonProperty("id")
    private long descriptionId;
    @JsonProperty("opfu_code")
    private int opfuCode;
    @JsonProperty("opfu_name")
    private String opfuName;
    @JsonProperty("date_cr")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "ddMMyyyy")
    private Date dateCr;
    @JsonProperty("MFO_filia")
    private int mfoFilia;
    @JsonProperty("filia_num")
    private int filiaNum;
    @JsonProperty("filia_name")
    private String filiaName;
    @JsonProperty("full_sum")
    private double fullSum;
    @JsonProperty("full_lines")
    private int fullLines;
    @JsonProperty("branches")
    private List<BranchXml> branches;

    public DescriptionXml() {};

    @SuppressWarnings("PMD.ExcessiveParameterList")
    public DescriptionXml(final long descriptionId, final int opfuCode, final String opfuName, final Date dateCr,
                          final int mfoFilia, final int filiaNum, final String filiaName, final double fullSum,
                          final int fullLines, final List<BranchXml> branches) {
        this.descriptionId = descriptionId;
        this.opfuCode = opfuCode;
        this.opfuName = opfuName;
        this.dateCr = dateCr;
        this.mfoFilia = mfoFilia;
        this.filiaNum = filiaNum;
        this.filiaName = filiaName;
        this.fullSum = fullSum;
        this.fullLines = fullLines;
        this.branches = branches;
    }

    public long getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(final long descriptionId) {
        this.descriptionId = descriptionId;
    }

    public int getOpfuCode() {
        return opfuCode;
    }

    public void setOpfuCode(final int opfuCode) {
        this.opfuCode = opfuCode;
    }

    public String getOpfuName() {
        return opfuName;
    }

    public void setOpfuName(final String opfuName) {
        this.opfuName = opfuName;
    }

    public Date getDateCr() {
        return dateCr;
    }

    public void setDateCr(final Date dateCr) {
        this.dateCr = dateCr;
    }

    public int getMfoFilia() {
        return mfoFilia;
    }

    public void setMfoFilia(final int mfoFilia) {
        this.mfoFilia = mfoFilia;
    }

    public int getFiliaNum() {
        return filiaNum;
    }

    public void setFiliaNum(final int filiaNum) {
        this.filiaNum = filiaNum;
    }

    public String getFiliaName() {
        return filiaName;
    }

    public void setFiliaName(final String filiaName) {
        this.filiaName = filiaName;
    }

    public double getFullSum() {
        return fullSum;
    }

    public void setFullSum(final double fullSum) {
        this.fullSum = fullSum;
    }

    public int getFullLines() {
        return fullLines;
    }

    public void setFullLines(final int fullLines) {
        this.fullLines = fullLines;
    }

    public List<BranchXml> getBranches() {
        return branches;
    }

    public void setBranches(final List<BranchXml> branches) {
        this.branches = branches;
    }
}
