package udc.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Description {

    @Id
    @Column(name = "id")
    private long descriptionId;
    @Column(name = "opfu_code")
    private int opfuCode;
    @Column(name = "opfu_name")
    private String opfuName;
    @Column(name = "date_cr")
    private Date dateCr;
    @Column(name = "MFO_filia")
    private int mfoFilia;
    @Column(name = "filia_num")
    private int filiaNum;
    @Column(name = "filia_name")
    private String filiaName;
    @Column(name = "full_sum")
    private double fullSum;
    @Column(name = "full_lines")
    private int fullLines;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, mappedBy="descriptionId")
    private List<Branch> branches;

    public Description() {};

    public Description(long descriptionId, int opfuCode, String opfuName, Date dateCr, int mfoFilia, int filiaNum,
                       String filiaName, double fullSum, int fullLines, List<Branch> branches) {
        this.descriptionId = descriptionId;
        this.opfuCode = opfuCode;
        this.opfuName = opfuName;
        this.dateCr = dateCr;
        this.mfoFilia = mfoFilia;
        this.filiaNum = filiaNum;
        this.filiaName = filiaName;
        this.fullSum = fullSum;
        this.fullLines = fullLines;
        this.branches = branches;
    }

    public long getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(long descriptionId) {
        this.descriptionId = descriptionId;
    }

    public int getOpfuCode() {
        return opfuCode;
    }

    public void setOpfuCode(int opfuCode) {
        this.opfuCode = opfuCode;
    }

    public String getOpfuName() {
        return opfuName;
    }

    public void setOpfuName(String opfuName) {
        this.opfuName = opfuName;
    }

    public Date getDateCr() {
        return dateCr;
    }

    public void setDateCr(Date dateCr) {
        this.dateCr = dateCr;
    }

    public int getMfoFilia() {
        return mfoFilia;
    }

    public void setMfoFilia(int mfoFilia) {
        this.mfoFilia = mfoFilia;
    }

    public int getFiliaNum() {
        return filiaNum;
    }

    public void setFiliaNum(int filiaNum) {
        this.filiaNum = filiaNum;
    }

    public String getFiliaName() {
        return filiaName;
    }

    public void setFiliaName(String filiaName) {
        this.filiaName = filiaName;
    }

    public double getFullSum() {
        return fullSum;
    }

    public void setFullSum(double fullSum) {
        this.fullSum = fullSum;
    }

    public int getFullLines() {
        return fullLines;
    }

    public void setFullLines(int fullLines) {
        this.fullLines = fullLines;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

}
