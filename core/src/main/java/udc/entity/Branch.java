package udc.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "branch_seq")
    @SequenceGenerator(name = "branch_seq", sequenceName = "branch_id_seq", initialValue = 100)
    private long branchId;
    @Column(name = "branch_num")
    private int branchNum;
    @Column(name = "branch_sum")
    private long branchSum;
    @Column(name = "branch_lines")
    private int branchLines;
    @Column(name = "date_pay")
    private Date datePay;
    @Column(name = "num_list")
    private int numList;
    @Column(name = "file_name")
    private String fileName;
    @Column(name = "file_data")
    private String fileData;
    @Column(name = "description_id")
    private long descriptionId;

    public Branch() {};

    public Branch(long branchId, int branchNum, long branchSum, int branchLines, Date datePay, int numList,
                  String fileName, String fileData, long descriptionId) {
        this.branchId = branchId;
        this.branchNum = branchNum;
        this.branchSum = branchSum;
        this.branchLines = branchLines;
        this.datePay = datePay;
        this.numList = numList;
        this.fileName = fileName;
        this.fileData = fileData;
        this.descriptionId = descriptionId;
    }

    public Branch(int branchNum, long branchSum, int branchLines, Date datePay, int numList,
                  String fileName, String fileData, long descriptionId) {
        this.branchNum = branchNum;
        this.branchSum = branchSum;
        this.branchLines = branchLines;
        this.datePay = datePay;
        this.numList = numList;
        this.fileName = fileName;
        this.fileData = fileData;
        this.descriptionId = descriptionId;
    }


    public long getBranchId() {
        return branchId;
    }

    public void setBranchId(long branchId) {
        this.branchId = branchId;
    }

    public int getBranchNum() {
        return branchNum;
    }

    public void setBranchNum(int branchNum) {
        this.branchNum = branchNum;
    }

    public long getBranchSum() {
        return branchSum;
    }

    public void setBranchSum(long branchSum) {
        this.branchSum = branchSum;
    }

    public int getBranchLines() {
        return branchLines;
    }

    public void setBranchLines(int branchLines) {
        this.branchLines = branchLines;
    }

    public Date getDatePay() {
        return datePay;
    }

    public void setDatePay(Date datePay) {
        this.datePay = datePay;
    }

    public int getNumList() {
        return numList;
    }

    public void setNumList(int numList) {
        this.numList = numList;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public long getDescriptionId() {
        return descriptionId;
    }

    public void setDescription(long descriptionId) {
        this.descriptionId = descriptionId;
    }
}
