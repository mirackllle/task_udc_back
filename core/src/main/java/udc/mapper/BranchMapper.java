package udc.mapper;

import org.springframework.stereotype.Component;
import udc.dto.BranchDto;
import udc.entity.Branch;

@Component
public class BranchMapper {

    public Branch dtoToEntity(final BranchDto branchDto) {
        return new Branch(
                branchDto.getBranchId(),
                branchDto.getBranchNum(),
                branchDto.getBranchSum(),
                branchDto.getBranchLines(),
                branchDto.getDatePay(),
                branchDto.getNumList(),
                branchDto.getFileName(),
                branchDto.getFileData(),
                branchDto.getDescriptionId());
    }
}
