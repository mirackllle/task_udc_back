package udc.mapper;

import org.springframework.stereotype.Component;
import udc.dto.BranchDto;
import udc.dto.DescriptionDto;
import udc.entity.Branch;
import udc.entity.Description;
import udc.xml.BranchXml;
import udc.xml.DescriptionXml;

import java.util.ArrayList;
import java.util.List;

@Component
public class DescriptionMapper {

    public Description xmlToEntity (final DescriptionXml desc) {
        final List<Branch> branches = new ArrayList<>();
        desc.getBranches().forEach(branchXml -> {
            branches.add(new Branch(
                    branchXml.getBranchNum(),
                    branchXml.getBranchSum(),
                    branchXml.getBranchLines(),
                    branchXml.getDatePay(),
                    branchXml.getNumList(),
                    branchXml.getFileName(),
                    branchXml.getFileData(),
                    desc.getDescriptionId()
            ));
        });
        return new Description(
                desc.getDescriptionId(),
                desc.getOpfuCode(),
                desc.getOpfuName(),
                desc.getDateCr(),
                desc.getMfoFilia(),
                desc.getFiliaNum(),
                desc.getFiliaName(),
                desc.getFullSum(),
                desc.getFullLines(),
                branches
        );
    }

    public DescriptionXml entityToXml (final Description description) {
        final List<BranchXml> branches = new ArrayList<>();
        description.getBranches().forEach(branch -> {
            branches.add(new BranchXml(
                    branch.getBranchNum(),
                    branch.getBranchSum(),
                    branch.getBranchLines(),
                    branch.getDatePay(),
                    branch.getNumList(),
                    branch.getFileName(),
                    branch.getFileData()
            ));
        });
        return new DescriptionXml(
                description.getDescriptionId(),
                description.getOpfuCode(),
                description.getOpfuName(),
                description.getDateCr(),
                description.getMfoFilia(),
                description.getFiliaNum(),
                description.getFiliaName(),
                description.getFullSum(),
                description.getFullLines(),
                branches
        );
    }

    public DescriptionDto entityToDto(final Description desc) {
        final List<BranchDto> branches = new ArrayList<>();
        desc.getBranches().forEach(branch -> {
            branches.add(new BranchDto(
                    branch.getBranchNum(),
                    branch.getBranchSum(),
                    branch.getBranchLines(),
                    branch.getDatePay(),
                    branch.getNumList(),
                    branch.getFileName(),
                    branch.getFileData(),
                    desc.getDescriptionId()
            ));
        });

        return new DescriptionDto(
                desc.getDescriptionId(),
                desc.getOpfuCode(),
                desc.getOpfuName(),
                desc.getDateCr(),
                desc.getMfoFilia(),
                desc.getFiliaNum(),
                desc.getFiliaName(),
                desc.getFullSum(),
                desc.getFullLines(),
                branches);
    }

}
