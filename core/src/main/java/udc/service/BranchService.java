package udc.service;

import udc.dto.BranchDto;
import udc.entity.Branch;

/**
 * Branch Service
 * @autor Prorock
 * @version 1.0
 */
public interface BranchService {

    /**
     * method insert or update branch
     * @param branch contains branch object
     * @return Branch
     */
    Branch save(BranchDto branch);

    /**
     * method delete branch
     * @param id contains id branch
     */
    void delete(long id);
}
