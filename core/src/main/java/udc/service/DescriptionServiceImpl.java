package udc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import udc.entity.Description;
import udc.repository.DescriptionRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Description Service implementation class
 * @autor Prorock
 * @version 1.0
 */
@Service
public class DescriptionServiceImpl implements DescriptionService {

    private final transient DescriptionRepository descriptionRep;

    @Autowired
    public DescriptionServiceImpl(final DescriptionRepository descriptionRep) {
        this.descriptionRep = descriptionRep;
    }

    @Override
    public List<Description> getAllDescription() {
        return descriptionRep.findAll();
    }

    @Override
    public Description getLastDescription() {
        return descriptionRep.findAll(Sort.by(Sort.Direction.ASC, "dateCr")).get(0);
    }

    @Override
    public Description getById(final long id) {
        return descriptionRep.findById(id).get();
    }

    @Override
    @Transactional
    public Description save(final Description description) {
        return descriptionRep.save(description);
    }
}
