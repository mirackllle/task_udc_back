package udc.service;

import org.springframework.stereotype.Service;
import udc.dto.BranchDto;
import udc.entity.Branch;
import udc.mapper.BranchMapper;
import udc.repository.BranchRepository;

import javax.transaction.Transactional;

/**
 * Branch Service implementation class
 * @autor Prorock
 * @version 1.0
 */
@Service
public class BranchServiceImpl implements BranchService {

    private final transient BranchRepository branchRepository;
    private final transient BranchMapper branchMapper;
    private final transient XmlService xmlService;

    public BranchServiceImpl(final BranchRepository branchRepository, final BranchMapper branchMapper,
                             final XmlService xmlService) {
        this.branchRepository = branchRepository;
        this.branchMapper = branchMapper;
        this.xmlService = xmlService;
    }

    @Override
    @Transactional
    public Branch save(final BranchDto branchDto) {
        final Branch branch = branchRepository.save(branchMapper.dtoToEntity(branchDto));
        xmlService.updateStoreFromDB();
        return branch;
    }

    @Override
    public void delete(final long id) {
        this.deleteById(id);
        xmlService.updateStoreFromDB();
    }

    @Transactional
    private void deleteById(final long id) {
        branchRepository.deleteById(id);
    }



}
