package udc.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import udc.config.XmlProperties;
import udc.mapper.DescriptionMapper;
import udc.message.Message;
import udc.xml.DescriptionXml;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;

/**
 * Xml Service implementation class
 * @autor Prorock
 * @version 1.0
 */
@Service
public class XmlServiceImpl implements XmlService {

    private static final Logger LOG = LoggerFactory.getLogger(XmlServiceImpl.class);

    private final transient XmlProperties xmlProperties;
    private final transient DescriptionService descriptionSer;
    private final transient DescriptionMapper descriptionMapper;

    @Autowired
    public XmlServiceImpl(final XmlProperties xmlProperties, final DescriptionService descriptionSer,
                          final DescriptionMapper descriptionMapper) {
        this.xmlProperties = xmlProperties;
        this.descriptionSer = descriptionSer;
        this.descriptionMapper = descriptionMapper;
    }

    @Override
    public boolean updateDBFromStore() {
        try {
            final InputStream resource = new ClassPathResource(xmlProperties.getFileUrl()).getInputStream();
            final XmlMapper xmlMapper = new XmlMapper();
            final String xml = inputStreamToString(resource);
            final DescriptionXml value = xmlMapper.readValue(xml, DescriptionXml.class);
            descriptionSer.save(descriptionMapper.xmlToEntity(value));
            return true;
        } catch (IOException e) {
            LOG.error(Message.ERROR_XML_TO_DATABASE.getDescription());
            LOG.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean updateStoreFromDB() {
        try {
            final StringWriter stringWriter = new StringWriter();
            final XMLStreamWriter sw = XMLOutputFactory.newFactory().createXMLStreamWriter(stringWriter);
            sw.writeStartDocument();
            new XmlMapper().writeValue(sw, descriptionMapper.entityToXml(descriptionSer.getLastDescription()));
            sw.writeEndDocument();
            final FileWriter file = new FileWriter(xmlProperties.getFileUrl());
            final BufferedWriter writer = new BufferedWriter(file);
            writer.write(stringWriter.toString());
            writer.close();
            return true;
        } catch (IOException | XMLStreamException e) {
            LOG.error(Message.ERROR_DATABASE_TO_XML.getDescription());
            LOG.error(e.getMessage());
        }
        return false;
    }

    private String inputStreamToString(final InputStream is) throws IOException {
        final StringBuilder sb = new StringBuilder();
        String line;
        final BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
