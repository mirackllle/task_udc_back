package udc.service;

/**
 * XML Service
 * @autor Prorock
 * @version 1.0
 */
public interface XmlService {

    /**
     * method update database from store
     * @return boolean
     */
    boolean updateDBFromStore();

    /**
     * method update store from database
     * @return boolean
     */
    boolean updateStoreFromDB();

}
