package udc.service;

import udc.entity.Description;

import java.util.List;

/**
 * Description Service
 * @autor Prorock
 * @version 1.0
 */
public interface DescriptionService {

    /**
     * method return all Description
     * @return List<Description>
     */
    List<Description> getAllDescription();

    /**
     * method return last Description
     * @return List<Description>
     */
    Description getLastDescription();

    /**
     * method return Description by id
     * @param id contains id description object
     * @return Description
     */
    Description getById(long id);

    /**
     * method insert or update branch
     * @param description contains description object
     * @return Description
     */
    Description save(Description description);

}
