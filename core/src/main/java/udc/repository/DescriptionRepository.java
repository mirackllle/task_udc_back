package udc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import udc.entity.Description;

@Repository
public interface DescriptionRepository extends JpaRepository<Description, Long> {
}
