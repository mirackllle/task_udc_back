package udc.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public class DescriptionDto {

    @NotNull
    private long descriptionId;
    @NotNull
    private int opfuCode;
    @NotEmpty
    private String opfuName;
    @NotNull
    private Date dateCr;
    @NotNull
    private int mfoFilia;
    @NotNull
    private int filiaNum;
    @NotEmpty
    private String filiaName;
    @NotNull
    private double fullSum;
    @NotNull
    private int fullLines;
    private List<BranchDto> branches;

    public DescriptionDto() {};

    public DescriptionDto(long descriptionId, int opfuCode, String opfuName, Date dateCr, int mfoFilia, int filiaNum,
                          String filiaName, double fullSum, int fullLines, List<BranchDto> branches) {
        this.descriptionId = descriptionId;
        this.opfuCode = opfuCode;
        this.opfuName = opfuName;
        this.dateCr = dateCr;
        this.mfoFilia = mfoFilia;
        this.filiaNum = filiaNum;
        this.filiaName = filiaName;
        this.fullSum = fullSum;
        this.fullLines = fullLines;
        this.branches = branches;
    }

    public long getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(long descriptionId) {
        this.descriptionId = descriptionId;
    }

    public int getOpfuCode() {
        return opfuCode;
    }

    public void setOpfuCode(int opfuCode) {
        this.opfuCode = opfuCode;
    }

    public String getOpfuName() {
        return opfuName;
    }

    public void setOpfuName(String opfuName) {
        this.opfuName = opfuName;
    }

    public Date getDateCr() {
        return dateCr;
    }

    public void setDateCr(Date dateCr) {
        this.dateCr = dateCr;
    }

    public int getMfoFilia() {
        return mfoFilia;
    }

    public void setMfoFilia(int mfoFilia) {
        this.mfoFilia = mfoFilia;
    }

    public int getFiliaNum() {
        return filiaNum;
    }

    public void setFiliaNum(int filiaNum) {
        this.filiaNum = filiaNum;
    }

    public String getFiliaName() {
        return filiaName;
    }

    public void setFiliaName(String filiaName) {
        this.filiaName = filiaName;
    }

    public double getFullSum() {
        return fullSum;
    }

    public void setFullSum(double fullSum) {
        this.fullSum = fullSum;
    }

    public int getFullLines() {
        return fullLines;
    }

    public void setFullLines(int fullLines) {
        this.fullLines = fullLines;
    }

    public List<BranchDto> getBranches() {
        return branches;
    }

    public void setBranches(List<BranchDto> branches) {
        this.branches = branches;
    }

}
