package udc.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class BranchDto {

    private long BranchId;
    @NotNull
    private int branchNum;
    @NotNull
    private long branchSum;
    @NotNull
    private int branchLines;
    @NotNull
    private Date datePay;
    @NotNull
    private int numList;
    @NotEmpty
    private String fileName;
    @NotEmpty
    private String fileData;
    @NotNull
    private long descriptionId;

    public BranchDto() {};

    public BranchDto(int branchNum, long branchSum, int branchLines, Date datePay, int numList,
                     String fileName, String fileData, long descriptionId) {
        this.branchNum = branchNum;
        this.branchSum = branchSum;
        this.branchLines = branchLines;
        this.datePay = datePay;
        this.numList = numList;
        this.fileName = fileName;
        this.fileData = fileData;
        this.descriptionId = descriptionId;
    }

    public long getBranchId() {
        return BranchId;
    }

    public void setBranchId(long BranchId) {
        this.BranchId = BranchId;
    }

    public int getBranchNum() {
        return branchNum;
    }

    public void setBranchNum(int branchNum) {
        this.branchNum = branchNum;
    }

    public long getBranchSum() {
        return branchSum;
    }

    public void setBranchSum(long branchSum) {
        this.branchSum = branchSum;
    }

    public int getBranchLines() {
        return branchLines;
    }

    public void setBranchLines(int branchLines) {
        this.branchLines = branchLines;
    }

    public Date getDatePay() {
        return datePay;
    }

    public void setDatePay(Date datePay) {
        this.datePay = datePay;
    }

    public int getNumList() {
        return numList;
    }

    public void setNumList(int numList) {
        this.numList = numList;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public long getDescriptionId() {
        return descriptionId;
    }

    public void setDescription(long descriptionId) {
        this.descriptionId = descriptionId;
    }
}
