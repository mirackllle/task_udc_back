package udc.filter;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class CustomRequestResponseLoggingFilterTest {

    private static final String TEST = "/test";
    private static final String GET = "GET";
    private static final String USERNAME = "USERNAME";

    @Mock
    Appender<ILoggingEvent> appender;

    private static final String REQ_BODY = "{\"data\":\"payload\"}";
    private static final String RESP_BODY = "{\"message\":\"message payload\"}";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void doFilterInternal_WithBodyLogging() throws ServletException, IOException {
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(appender);

        var mockFilterChain = new MockFilterChain();

        var mockHttpServletRequest = prepareRequest();

        var mockHttpServletResponse = prepareResponse();

        var filterWithBody = new CustomRequestResponseLoggingFilter(true);

        filterWithBody.doFilterInternal(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

        var eventArgumentCaptor = ArgumentCaptor.forClass(ILoggingEvent.class);
        verify(appender, times(4)).doAppend(eventArgumentCaptor.capture());

        var allValues = eventArgumentCaptor.getAllValues();

        assertThat(allValues.size()).isEqualTo(4);
        assertThat(allValues.get(0).getMessage()).contains(TEST, GET, USERNAME);
        assertThat(allValues.get(1).getMessage()).contains(HttpStatus.OK.toString(),
                MediaType.APPLICATION_JSON_UTF8_VALUE);
    }

    @Test
    void doFilterInternal_WithoutBodyLogging() throws ServletException, IOException {
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(appender);

        var mockFilterChain = new MockFilterChain();

        var mockHttpServletRequest = prepareRequest();

        var mockHttpServletResponse = prepareResponse();

        var filterWithoutBody = new CustomRequestResponseLoggingFilter(false);

        filterWithoutBody.doFilterInternal(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

        var eventArgumentCaptor = ArgumentCaptor.forClass(ILoggingEvent.class);
        verify(appender, times(2)).doAppend(eventArgumentCaptor.capture());

        var allValues = eventArgumentCaptor.getAllValues();

        assertThat(allValues.get(0).getMessage().toString()).contains(TEST, GET, USERNAME);
        assertThat(allValues.get(1).getMessage().toString()).contains(HttpStatus.OK.toString(),
                MediaType.APPLICATION_JSON_UTF8_VALUE);
    }

    @Test
    void doFilterInternal_WithoutBodyLogging_NoPrincipal() throws ServletException, IOException {

        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(appender);

        var mockFilterChain = new MockFilterChain();

        var requestWithoutPrincipal = prepareRequestWithoutPrincipal();

        var mockHttpServletResponse = prepareResponse();

        var filterWithoutBody = new CustomRequestResponseLoggingFilter(false);

        filterWithoutBody.doFilterInternal(requestWithoutPrincipal, mockHttpServletResponse, mockFilterChain);

        var eventArgumentCaptor = ArgumentCaptor.forClass(ILoggingEvent.class);
        verify(appender, times(2)).doAppend(eventArgumentCaptor.capture());

        var allValues = eventArgumentCaptor.getAllValues();

        assertThat(allValues.get(0).getMessage().toString()).contains(TEST, GET, "null");
        assertThat(allValues.get(1).getMessage().toString()).contains(HttpStatus.OK.toString(),
                MediaType.APPLICATION_JSON_UTF8_VALUE);

    }

    private MockHttpServletRequest prepareRequest() {
        var mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.setRequestURI(TEST);
        mockHttpServletRequest.setMethod(GET);
        mockHttpServletRequest.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        mockHttpServletRequest.setUserPrincipal(() -> USERNAME);
        mockHttpServletRequest.setContent(REQ_BODY.getBytes());
        mockHttpServletRequest.setCharacterEncoding("UTF-8");
        return mockHttpServletRequest;
    }

    private MockHttpServletRequest prepareRequestWithoutPrincipal() {
        var mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.setRequestURI(TEST);
        mockHttpServletRequest.setMethod(GET);
        mockHttpServletRequest.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        mockHttpServletRequest.setUserPrincipal(null);
        mockHttpServletRequest.setContent(REQ_BODY.getBytes());
        mockHttpServletRequest.setCharacterEncoding("UTF-8");
        return mockHttpServletRequest;
    }


    private MockHttpServletResponse prepareResponse() {
        var mockHttpServletResponse = new MockHttpServletResponse();
        mockHttpServletResponse.setStatus(HttpStatus.OK.value());
        mockHttpServletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        try {
            mockHttpServletResponse.getWriter().write(RESP_BODY);
            mockHttpServletResponse.getWriter().flush();
            mockHttpServletResponse.getWriter().close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return mockHttpServletResponse;
    }

}