package udc.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JsonParserUtils {

    public static JSONObject parseJsonToObject(String pathToFile) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = new File(ClassLoader.getSystemResource(pathToFile).getFile());
        Object obj = parser.parse(new FileReader(file));
        return (JSONObject) obj;
    }

    public static JSONArray parseJsonArrayToArrayOfObjects(String pathToFile) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = new File(ClassLoader.getSystemResource(pathToFile).getFile());
        Object obj = parser.parse(new FileReader(file));
        return (JSONArray) obj;
    }
}
