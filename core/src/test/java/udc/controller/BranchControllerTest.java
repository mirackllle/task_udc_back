package udc.controller;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import udc.ApplicationRunner;
import udc.utils.JsonParserUtils;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        classes = ApplicationRunner.class,
        webEnvironment = SpringBootTest.WebEnvironment.MOCK
)
@AutoConfigureMockMvc
class BranchControllerTest {

    private final String brachJson = "json/branch.json";
    private final String wrongBranchJson = "json/wrongBranch.json";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .build();
    }

    @Test
    void insertBranch() throws Exception {
        JSONObject jsonObject = JsonParserUtils.parseJsonToObject(brachJson);
        String jsonSource = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/branch/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSource))
                .andExpect(status().is(200));
    }

    @Test
    void insertBranchWithError() throws Exception {
        JSONObject jsonObject = JsonParserUtils.parseJsonToObject(wrongBranchJson);
        String jsonSource = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/branch/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSource))
                .andExpect(status().is(400));
    }

    @Test
    void deleteBranch() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/branch/delete/109")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }

    @Test
    void deleteBranchError() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/branch/delete/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404));
    }

    @Test
    void updateBranch() throws Exception {
        JSONObject jsonObject = JsonParserUtils.parseJsonToObject(brachJson);
        String jsonSource = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.put("/branch/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSource))
                .andExpect(status().is(200));
    }

    @Test
    void updateBranchError() throws Exception {
        JSONObject jsonObject = JsonParserUtils.parseJsonToObject(wrongBranchJson);
        String jsonSource = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.put("/branch/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSource))
                .andExpect(status().is(400));
    }
}