package udc.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import udc.dto.BranchDto;
import udc.entity.Branch;
import udc.mapper.BranchMapper;
import udc.repository.BranchRepository;

import java.util.Date;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

class BranchServiceImplTest {

    private BranchDto branchDto;
    private Branch branch;

    private BranchRepository branchRepository;
    private BranchMapper branchMapper;
    private XmlService xmlService;
    private BranchService branchService;

    @BeforeEach
    void init() {
        branchRepository = mock(BranchRepository.class);
        branchMapper = mock(BranchMapper.class);
        xmlService = mock(XmlServiceImpl.class);
        branchService = new BranchServiceImpl(branchRepository, branchMapper, xmlService);

        branchDto = new BranchDto();
        branchDto.setBranchId(1);
        branchDto.setBranchLines(1);
        branchDto.setBranchNum(1);
        branchDto.setBranchSum(1);
        branchDto.setDatePay(new Date());
        branchDto.setFileData("1");
        branchDto.setFileName("1");
        branchDto.setNumList(1);
        branchDto.setDescription(1);

        branch = new Branch();
        branch.setBranchId(1);
        branch.setBranchLines(1);
        branch.setBranchNum(1);
        branch.setBranchSum(1);
        branch.setDatePay(new Date());
        branch.setFileData("1");
        branch.setFileName("1");
        branch.setNumList(1);
        branch.setDescription(1);
    }

    @Test
    void deleteBranchById() {
        long id = 100;
        doNothing().when(branchRepository).deleteById(id);
        when(xmlService.updateStoreFromDB()).thenReturn(true);
        branchService.delete(id);
        verify(xmlService, times(1)).updateStoreFromDB();
    }

    @Test
    void saveBranch() {
        when(branchMapper.dtoToEntity(branchDto)).thenReturn(branch);
        when(branchRepository.save(branch)).thenReturn(branch);
        when(xmlService.updateStoreFromDB()).thenReturn(true);
        Branch res = branchService.save(branchDto);
        assertEquals(res, branch);
    }
}