package udc.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import udc.config.XmlProperties;
import udc.entity.Description;
import udc.mapper.DescriptionMapper;
import udc.xml.DescriptionXml;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class XmlServiceImplTest {

    private Description description = new Description(1, 1, "1", new Date(), 1, 1, "1", 1, 1, new ArrayList());
    private DescriptionXml descriptionXml = new DescriptionXml(1, 1, "1", new Date(), 1, 1, "1", 1, 1, new ArrayList());

    private XmlServiceImpl xmlService;
    private DescriptionService descriptionService;
    private DescriptionMapper descriptionMapper;
    private XmlProperties xmlProperties;

    @BeforeEach
    void init() {
        xmlProperties = mock(XmlProperties.class);
        descriptionService = mock(DescriptionService.class);
        descriptionMapper = mock(DescriptionMapper.class);
        xmlService = new XmlServiceImpl(xmlProperties, descriptionService, descriptionMapper);
    }

    @Test
    void successfulyUpdatedDBFromStore() {
        when(xmlProperties.getFileUrl()).thenReturn("store.xml");
        assertTrue(xmlService.updateDBFromStore());
    }

    @Test
    void errorUpdatedDBFromStore() {
        when(xmlProperties.getFileUrl()).thenReturn("tore.xml");
        assertFalse(xmlService.updateDBFromStore());
    }

    @Test
    void successfulyUpdatedStoreFromDB() {
        when(xmlProperties.getFileUrl()).thenReturn("store.xml");
        when(descriptionService.getLastDescription()).thenReturn(description);
        when(descriptionMapper.entityToXml(description)).thenReturn(descriptionXml);
        assertTrue(xmlService.updateStoreFromDB());
    }

    @Test
    void errorUpdateStoreFromDB() {
        when(xmlProperties.getFileUrl()).thenReturn("{:/classpath: orexml");
        assertFalse(xmlService.updateStoreFromDB());
    }
}