package udc.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Sort;
import udc.entity.Branch;
import udc.entity.Description;
import udc.repository.DescriptionRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DescriptionServiceImplTest {

    private DescriptionRepository descriptionRepository;
    private DescriptionService descriptionService;
    private Branch branch;
    private Description description;

    @BeforeEach
    void init() {
        descriptionRepository = mock(DescriptionRepository.class);
        descriptionService = new DescriptionServiceImpl(descriptionRepository);

        List list = new ArrayList<>();
        branch = new Branch();
        branch.setBranchId(1);
        branch.setBranchLines(1);
        branch.setBranchNum(1);
        branch.setBranchSum(1);
        branch.setDatePay(new Date());
        branch.setFileData("1");
        branch.setFileName("1");
        branch.setNumList(1);
        branch.setDescription(1);

        list.add(branch);

        description = new Description();
        description.setBranches(list);
        description.setDateCr(new Date());
        description.setDescriptionId(1);
        description.setFiliaName("1");
        description.setFiliaNum(1);
        description.setFullLines(1);
        description.setFullSum(1);
        description.setMfoFilia(1);
        description.setOpfuCode(1);
        description.setOpfuName("1");
    }

    @Test
    void testGetAllDescription() {
        List list = new ArrayList<>();
        list.add(description);
        when(descriptionRepository.findAll()).thenReturn(list);
        assertEquals(list.get(0), descriptionService.getAllDescription().get(0));
    }

    @Test
    void testGetLastDescription() {
        List list = new ArrayList<>();
        list.add(description);
        when(descriptionRepository.findAll(Mockito.any(Sort.class))).thenReturn(list);
        assertEquals(list.get(0), descriptionService.getLastDescription());
    }

    @Test
    void testGetById() {
        long id = 100;
        when(descriptionRepository.findById(id)).thenReturn(Optional.of(description));
        assertEquals(description, descriptionService.getById(id));
    }

    @Test
    void testSave() {
        when(descriptionRepository.save(description)).thenReturn(description);
        assertEquals(description, descriptionService.save(description));
    }
}